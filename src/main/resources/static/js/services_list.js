$(document).ready(function ($) {
  // make table rows clickable
  $(".clickable-row").click(function () {
    window.location = $(this).data("href");
  });

  const table = $("#services-table");
  if (tableLocale !== "en") {
    table.DataTable({
      language: {
        url: tableI18nLocation,
      },
    });
  } else {
    table.DataTable();
  }
});
